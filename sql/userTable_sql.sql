CREATE DATABASE usermanagement  DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user(
    id SERIAL PRIMARY KEY AUTO_INCREMENT,
    login_id varchar(255) UNIQUE NOT NULL,
    name varchar(255) UNIQUE,
    birth_date DATE NOT NULL,
    password varchar(255) NOT NULL,
    is_admin boolean NOT NULL DEFAULT false,
    create_date DATETIME NOT NULL,
    update_date DATETIME NOT NULL 
    );
    
INSERT INTO user(login_id,name,birth_date,password,is_admin,create_date,update_date)VALUES
('admin','�Ǘ���','1990/09/30','password',true,NOW(),NOW());
    
