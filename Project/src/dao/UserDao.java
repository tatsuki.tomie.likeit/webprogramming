package dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import model.User;

public class UserDao {
	
	public User findByLoginInfo(String loginId, String password) {
	Connection conn =null;
	 try {
	conn = DBManager.getConnection();
	String sql =  "SELECT * FROM user WHERE login_id = ? and password = ?";
	 PreparedStatement pStmt = conn.prepareStatement(sql);
     pStmt.setString(1, loginId);
     pStmt.setString(2, password);
     ResultSet rs = pStmt.executeQuery();

   if(!rs.next()) {
	   return null;
   }
	
	String loginIdData = rs.getString("login_id");
	String nameData = rs.getString("name");
	return new User(loginIdData, nameData);
	

	 }catch (SQLException e) {
       e.printStackTrace();
       return null;
   } finally {

       if (conn != null) {
           try {
               conn.close();
           } catch (SQLException e) {
               e.printStackTrace();
               return null;
           }
       }
   }

   
}

public User findById(int id) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "SELECT * FROM user WHERE id= ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setInt(1, id);

		ResultSet rs = pStmt.executeQuery();
		
		if (!rs.next()) {
			return null;
		}
	    String loginId = rs.getString("login_id");
		String name = rs.getString("name");
		Date birthDate = rs.getDate("birth_date");
		String password = rs.getString("password");
		boolean isAdmin = rs.getBoolean("is_admin");
		Timestamp createDate = rs.getTimestamp("create_date");
		Timestamp updateDate = rs.getTimestamp("update_date");
		User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);
		
		return user;

				
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}


}


public List<User> findAll() {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();

	try {

		conn = DBManager.getConnection();


		String sql = "SELECT * FROM user WHERE is_admin= false";


		PreparedStatement pStmt = conn.prepareStatement(sql);

		ResultSet rs = pStmt.executeQuery();


		while (rs.next()) {
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

			userList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return userList;
}

public List<User> search(String userLoginId, String userName, String startDate, String endDate) {
	Connection conn = null;
	List<User> userList = new ArrayList<User>();

	try {
		List<String> searchList = new ArrayList<>();
        conn = DBManager.getConnection();

		String sql = "SELECT * FROM user WHERE is_admin= false";



		StringBuilder stringBuilder = new StringBuilder(sql);


		if (!(userLoginId.equals(""))) {
			stringBuilder.append(" and login_id = ?");
      	searchList.add(userLoginId);
	}

	if (!(userName.equals(""))) {
		stringBuilder.append(" and name like ?");
		searchList.add("%" + userName + "%");

	}

	if (!(startDate.equals(""))) {
		stringBuilder.append(" and birth_date >= ?");
		searchList.add(startDate);

	}

	if (!(endDate.equals(""))) {
		stringBuilder.append(" and birth_date <= ?");
		searchList.add(endDate);

	}

	PreparedStatement pStmt = conn.prepareStatement(stringBuilder.toString());
      for(int i = 0;i < searchList.size();i++){
			pStmt.setString(i + 1, searchList.get(i));
	}




		ResultSet rs = pStmt.executeQuery();

		while (rs.next()) {
			int id = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			boolean isAdmin = rs.getBoolean("is_admin");
			Timestamp createDate = rs.getTimestamp("create_date");
			Timestamp updateDate = rs.getTimestamp("update_date");
			User user = new User(id, loginId, name, birthDate, password, isAdmin, createDate, updateDate);

			userList.add(user);
		}
	} catch (SQLException e) {
		e.printStackTrace();
		return null;
	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				return null;
			}
		}
	}
	return userList;
}



public void userUpdate(String id, String password, String name, String birthDate) {

	Connection conn = null;
	PreparedStatement pStmt = null;

	try {
		conn = DBManager.getConnection();

		String sql = "UPDATE user SET name = ?,password = ?,birth_date = ?,update_date = now() WHERE id = ?";

		pStmt = conn.prepareStatement(sql);

		pStmt.setString(1, name);
		pStmt.setString(2, password);
		pStmt.setString(3, birthDate);
		pStmt.setString(4, id);

		pStmt.executeUpdate();

		pStmt.close();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}

	}

}


public void userAdd(String loginId, String name, String birthDate, String password) {
	
	Connection conn =null;
	PreparedStatement pStmt = null;

	
	 try {
	conn = DBManager.getConnection();
	
	String sql = "INSERT INTO user (login_id,name,birth_date,password,create_date,update_date)VALUES(?,?,?,?,now(),now())";
   
	pStmt = conn.prepareStatement(sql);

	pStmt.setString(1, loginId);
	pStmt.setString(2, name);
	pStmt.setString(3, birthDate);
	pStmt.setString(4, password);

	pStmt.executeUpdate();

	pStmt.close();
} catch (SQLException e) {
	e.printStackTrace();

} finally {
	if (conn != null) {
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();


		}
	}

}
}


public void userDelete(String id) {
	Connection conn = null;
	try {
		conn = DBManager.getConnection();
		String sql = "DELETE FROM user WHERE id = ?";
		PreparedStatement pStmt = conn.prepareStatement(sql);
		pStmt.setString(1, id);

		pStmt.executeUpdate();

		pStmt.close();
	} catch (SQLException e) {
		e.printStackTrace();

	} finally {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();

			}
		}
	}
}
}

