package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserDetailServlet")
public class UserDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserDetailServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	

	HttpSession session = request.getSession();
	User user = (User) session.getAttribute("userInfo");

	if(user==null) {
		response.sendRedirect("LoginServlet");
	   
		return;
	}

	String userId = request.getParameter("id");
	int Id = Integer.parseInt(userId);
	
	UserDao userDao = new UserDao();
	User userDetail = userDao.findById(Id);

	request.setAttribute("userDetail", userDetail);
	
	RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDetail.jsp");
	dispatcher.forward(request, response);
	

}
}
