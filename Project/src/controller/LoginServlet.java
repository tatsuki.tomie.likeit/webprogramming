package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;
import util.PasswordEncorder;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public LoginServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String loginId = request.getParameter("inputLoginId");
		String password = request.getParameter("password");



	UserDao userDao = new UserDao();
	String encodeStr = PasswordEncorder.encordPassword(password);
	User user = userDao.findByLoginInfo(loginId, encodeStr);

	if (user == null) {
		request.setAttribute("errMsg", "ログインIDまたはパスワードが異なります");
		request.setAttribute("loginId", loginId);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userLogin.jsp");
		dispatcher.forward(request, response);
		return;

	}

	HttpSession session = request.getSession();
	session.setAttribute("userInfo", user);
	response.sendRedirect("UserListServlet");
}
}
