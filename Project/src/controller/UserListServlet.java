package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserListServlet() {
		super();
	}


	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");

		if (user == null) {
			response.sendRedirect("LoginServlet");

			return;
		}
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		request.setAttribute("userList", userList);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String userLoginId = request.getParameter("login-id");
		String userName = request.getParameter("user-name");
		String startDate = request.getParameter("start-date");
		String endDate = request.getParameter("end-date");

		UserDao userDao = new UserDao();
		List<User> userList = userDao.search(userLoginId, userName, startDate, endDate);

		request.setAttribute("userList", userList);
		request.setAttribute("loginId", userLoginId);
		request.setAttribute("name", userName);
		request.setAttribute("birthDate", startDate);
		request.setAttribute("birthDate2", endDate);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userList.jsp");
		dispatcher.forward(request, response);

	}
}
